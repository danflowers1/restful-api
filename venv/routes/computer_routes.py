from flask import Blueprint, jsonify, request
from logger.logger_base import log

class ComputerRoutes(Blueprint):

    def __init__(self, computer_service, computer_schema):
        super().__init__('computer', __name__)
        self.computer_service = computer_service
        self.computer_schema = computer_schema
        self.register_routes()

    def register_routes(self):
        self.route('/RESTful_API/computers', methods=['GET'])(self.get_computers)
        self.route('/RESTful_API/computers/<int:computer_id>', methods=['GET'])(self.get_computer_by_id)
        self.route('/RESTful_API/computers', methods=['POST'])(self.insert_computer)
        self.route('/RESTful_API/computers/<int:computer_id>', methods=['PUT'])(self.update_computer)
        self.route('/RESTful_API/computers/<int:computer_id>', methods=['DELETE'])(self.delete_computer)
    

    def get_computers(self):
        try:
            computers = self.computer_service.get_all_computers()
            return jsonify(computers), 200
        except Exception as e:
            log.exception(f'Error fetching data from the database: {e}')
            return jsonify({'error' : 'Failed to fetch data from the database.'}), 500
        
    def get_computer_by_id(self, computer_id):
        computer = self.computer_service.get_computer_by_id(computer_id)
        if computer:
            return jsonify(computer), 200
        else:
            return jsonify({'error' : 'Computer not found'}), 404
        

    def insert_computer(self):
        try:
            computer_json = request.get_json()
            self.computer_schema.dump(computer_json)
            self.computer_service.create_computer(computer_json)
            return computer_json, 200
        except Exception as e:
            log.exception(f'Error inserting data into the database: {e}')
            return jsonify({"error" : "Computer couldn't be inserted."}), 500
        

    def update_computer(self, computer_id):
        try:
            updated_computer = request.get_json()
            self.computer_schema.dump(updated_computer)
            self.computer_service.update_computer(computer_id, updated_computer)
            return jsonify({"message": "Computer updated successfully"}), 200
        except Exception as e:
            log.exception(f"Error updating the record: {e}")
            return jsonify({"error" : "Computer couldn't be updated."}), 500

        

    def delete_computer(self, computer_id):
        try:
            return jsonify({'_id' : self.computer_service.delete_computer(computer_id)}), 200
        except Exception as e:
            log.exception(f"Error deleting the record: {e}")
            return jsonify({"error" : "Computer couldn't be deleted."}), 500