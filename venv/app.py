from flask import Flask
from models.computer_models import ComputerModel
from services.computer_services import ComputerService
from routes.computer_routes import ComputerRoutes
from schema.computer_schema import ComputerSchema
from flask_swagger_ui import get_swaggerui_blueprint

app = Flask(__name__)

db_connector = ComputerModel(dbname='RESTful_API')
db_connector.connect_to_database()
computer_service = ComputerService(db_connector)
computer_blueprint = ComputerRoutes(computer_service, computer_schema=ComputerSchema())
app.register_blueprint(computer_blueprint)

if __name__ == "__main__":
    app.run()