from logger.logger_base import log
from flask import jsonify
from pymongo import MongoClient 

class ComputerService:
    def __init__(self, db_connector) -> None:
        self.db_connector = db_connector

    def get_all_computers(self):
        try:
            computers = list(self.db_connector.db.computers.find())
            return computers
        except Exception as e:
            log_message = f'Error fetching all computers from the database: {e}'
            log.critical(log_message)
            return jsonify({'error': log_message}), 500
    
    def get_computer_by_id(self, computer_id):
        try:
            computer = self.db_connector.db.computers.find_one({'_id': computer_id})
            if computer:
                return computer
            else:
                return jsonify({'error': 'Computer not found'}), 404
        except Exception as e:
            log_message = f'Error fetching computer by id from the database: {e}'
            log.critical(log_message)
            return jsonify({'error': log_message}), 500
        
    def create_computer(self, computer):
        try:
            self.db_connector.db.Computers.insert_one(computer)
            return computer
        except Exception as e:
            log.critical(f'The computer failed to be inserted into the database: {e}')
            return  jsonify({'error' : "The computer couldn't be inserted intpo the database."})
    
    def update_computer(self, computer_id, updated_computer):
        try:
            self.db_connector.db.Computers.update_one({"_id" : computer_id}, {"$set": updated_computer})
            return self.get_computer_by_id(computer_id)
        except Exception as e:
            log.critical(f"The specified system record couldn't be updated: {e}")
            return jsonify({'error' : "The specified system record couldn't be updated."})
           
    def delete_computer(self, computer_id):
        try:
            self.db_connector.db.Computers.delete_one({"_id" : computer_id})
            return computer_id
        except Exception as e:
            log.critical(f"The specified system record couldn't be deleted: {e}")
            return jsonify({'error' : "The specified system record couldn't be deleted."})

  
if __name__ == '__main__':
    from models.computer_models import ComputerModel
    dbConnector = ComputerModel()
    dbConnector.connect_to_database()
    cs = ComputerService(dbConnector)
    print(cs.delete_computer(5))
    dbConnector.close_connection()