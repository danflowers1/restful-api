from marshmallow import Schema, fields, validates, ValidationError
from enum import Enum
from logger.logger_base import log
from datetime import datetime

class ComputerSchema(Schema):
    
    _id = fields.Int(required=True)
    name = fields.String(required=True)
    type = fields.String(required=True)
    marca = fields.String(required=True)
    modelo = fields.String(required=True)
    serial_number = fields.String(required=True)
    state = fields.String(required=True)
    register_date = fields.String(required=True)
    location = fields.String(required=True)
    technical_details = fields.Dict(required=True)
    
    computerTypeEnum = Enum('Type', ['Laptop', 'Desktop'])
    computerStateEnum = Enum('State', ['active', 'inactive'])


    @validates('name')
    def validate_title(self, value):
        if len(value) <= 5:
            raise ValidationError("name must be at least 5 chraacters long.")
        
    @validates('type')
    def validate_autor(self,value):
        if len(value) < 5:
            raise ValidationError("type must be at least 5 characters long.")
        
    @validates('brand')
    def validate_brand(self,value):
        if len(value) < 5:
            raise ValidationError("brand must be at least 5 characters long.")
    
    @validates('serial_number')
    def validate_serial_number(self,value):
        if len(value) < 5:
            raise ValidationError("serial_number must be at least 5 characters long.")
        
    @validates('state')
    def validate_state(self,value):
        if len(value) < 5:
            raise ValidationError("state must be at least 5 characters long.")
    
    @validates('register_day')
    def validate_register_day(self,value):
        if len(value) < 5:
            raise ValidationError("resgister day must be at least 5 characters long.")
    
    @validates('location')
    def validate_location(self,value):
        if len(value) < 5:
            raise ValidationError("location must be at least 5 characters long.")
        
    @validates('model')
    def validate_brand(self,value):
        if len(value) < 5:
            raise ValidationError("model must be at least 5 characters long.")
        
if __name__ == '__main__':
    computer_schema = ComputerSchema()
    example = {
    "_id": 7,
    "name": "Zenbook",
    "type": "Laptop",
    "marca": "Asus",
    "modelo": "24-inch",
    "serial_number": "AS-100",
    "state": "active",
    "register_date": "2024-03-30",
    "location": "Design Department",
    "technical_details": {
      "processor": "Ryzen 7",
      "ram": "8GB",
      "storage": "512GB SSD",
      "operating_sysem": "Windows 11"
    }
  }
    try:
        obj = computer_schema.dump(example)
        print("The object is valid.")
        print(obj)
    except Exception as e:
        log.critical(f"The inputted format is invalid: {e}")